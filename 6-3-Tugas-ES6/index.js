// soal 1

const luas_keliling = (a, b) => {
    return ("luas persegi panjang = " + a * b + " dan keliling persegi panjang = " + 2 * ( a + b));
}

console.log(luas_keliling(6, 2));
// expected output: luas persegi panjang = 12 dan keliling persegi panjang = 16

// Soal 2

const newFunction = (firstName, lastName) => {
        return {
          firstName,
          lastName,
          fullName: function(){
               console.log(`${firstName}  ${lastName}`)
              }
        }
      }
      newFunction("William", "Imoh").fullName()

// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)

// Soal 5
const planet = "earth" 
const view = "glass" 

console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`)