// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()

for(var i = 0; i <= 5; i++){
    console.log(daftarHewan[i])
}
    
    
//soal 2
function introduce(data){
    return ("Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby + "!");

}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan)

//soal 3

function hitung_huruf_vokal(str) {
	var vokal = 'aeiouAEIOU';
	var count = 0;

	for (var x = 0; x < str.length; x++) {
		if (vokal.indexOf(str[x]) !== -1) {
			count++;
		}
	}
	return count;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2)

//soal 4
function hitung (angka)
{
    for(var i = 0; i <= 5; i++) {
        return angka - 2 + angka ;
   }
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8