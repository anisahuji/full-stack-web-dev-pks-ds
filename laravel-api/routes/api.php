<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * route resource posts
 */
Route::apiResource('/posts', 'PostsController');

/**
 * route resource roles
 */
Route::apiResource('/roles', 'RolesController');

/**
 * route resource comments
 */
Route::apiResource('/comments', 'CommentsController');

Route::group([
    'prefix' => 'auth' ,
    'namespace' => 'Auth'
 ] , function(){
    Route::post('register' , 'RegisterController')->name('auth.register');
    Route::post('regerate-otp-code' , 'RegisterController')->name('auth.regenarate_otp_code');
    Route::post('verification' , 'VerificationController')->name('auth.verification');
    Route::post('update-password' , 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login' , 'LoginController')->name('auth.login');
}
);

