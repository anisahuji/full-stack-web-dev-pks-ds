<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
   /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $posts = Posts::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data'    => $posts  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $posts = Posts::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $posts 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $users = auth()->user();

        //save to database
        $posts = Posts::create([
            'title'     => $request->title,
            'user_id' => $users->id,
        ]);

        //success save to database
        if($posts) {

            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data'    => $posts  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    
    
     public function update(Request $request, Posts $posts)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $posts = Posts::findOrFail($posts->id);

        $users = auth()->users();

        if($posts->user_id != $users->id) {

            return response()->json([
                'success' => false,
                'message' => 'Data posts bukan milik user login',
                'data'    => $posts  
            ], 403);
            //update post
            $posts->update([
                'title'     => $request->title
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data'    => $posts  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $posts = Posts::findOrfail($id);

        if($posts) {
            $users = auth()->users();

            if($posts->user_id != $users->id) {
    
                return response()->json([
                    'success' => false,
                    'message' => 'Data posts bukan milik user login',
                    'data'    => $posts  
                ], 403);
                
            //delete post
            $posts->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
        }
    }
}
