<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Events\CommentStoredEvent; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table comments
        $comments = Comments::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comments',
            'data'    => $comments  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find comments by ID
        $comments = Comments::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comments',
            'data'    => $comments 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $users = auth()->user();

        //save to database
        $comments = Comments::create([
            'content'     => $request->content,
            'post_id'     => $request->post_id,
            'user_id' => $users->id,
        ]);

        // memanggil event CommentStoredEvent
        event(new CommentSoredEvent($comments));

        // Mail::to($comments->posts->users->email)->send(new PostAuthorMail($comments));
        
        // Mail::to($comments->users->email)->send(new CommentAuthorMail($comments));
        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'Comments Created',
                'data'    => $comments  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comments Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Comments $comments)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comments = Comments::findOrFail($comments->id);

        $users = auth()->users();

        if($comments->user_id != $users->id) {

            return response()->json([
                'success' => false,
                'message' => 'Data comments bukan milik user login',
                'data'    => $comments  
            ], 403);
            //update comment
            $comments->update([
                'content'     => $request->content,
                'post_id'     => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comments  
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find comment by ID
        $comments = Comments::findOrfail($id);

        if($comments) {
            $users = auth()->users();

            if($comments->user_id != $users->id) {
    
                return response()->json([
                    'success' => false,
                    'message' => 'Data comments bukan milik user login',
                    'data'    => $comments  
                ], 403);
            //delete comment
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comments Deleted',
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'Comments Not Found',
        ], 404);
    }
}
}
